<?php

    class segiEmpat{

        private float $panjang;
        private float $lebar;

        public function setPanjang($p){
            $this->panjang = $p;
        }

        public function getPanjang(){
            return $this->panjang;
        }
        public function setLebar($l){
            $this->lebar = $l;
        }
        public function getLebar(){
            return $this->lebar;
        }

        public function hitungLuas()
        {
            return $this->getPanjang() * $this->getLebar();
        }
    }

    class Kubus extends segiEmpat{
        private $volume;
        function setVolume($sisi){

            $this->setPanjang($sisi);
            $this->setLebar($sisi);
            $luas = $this->hitungLuas();
            $this->volume = $luas * $this->getPanjang();
        }

        function getVolume(){
            return $this->volume;
        }
    }

    class Balok extends segiEmpat{
        private $volume;
        function setVolume($panjang, $lebar, $tinggi){

            $this->setPanjang($panjang);
            $this->setLebar($lebar);
            $luas = $this->hitungLuas();
            $this->volume = $luas * $tinggi;
        }

        function getVolume(){
            return $this->volume;
        }
    }
    

    class Hasil{
        function tampilKubus($sisi){
            $hasilVolume = new Kubus();
            $hasilVolume->setVolume($sisi);
            echo "Volume Kubus dengan panjang sisi $sisi adalah ". $hasilVolume->getVolume(). "<br>";
        }

        function tampilBalok($panjang, $lebar, $tinggi){
            $hasilVolume = new Balok();
            $hasilVolume->setVolume($panjang, $lebar, $tinggi);
            echo "Volume Balok dengan panjang $panjang, lebar $lebar, dan tinggi $tinggi adalah ". $hasilVolume->getVolume(). "<br>";
        }
    }

    $hasil = new Hasil();
    $hasil->tampilKubus(4);
    $hasil->tampilBalok(3,4,5);
    

    
?> 