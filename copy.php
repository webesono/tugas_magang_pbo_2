<?php

interface bangunDatar{
    public function hitungLuas();
    public function hitungKeliling();
}

    class segiEmpat implements bangunDatar{

        private float $panjang;
        private float $lebar;

        public function setPanjang($p){
            $this->panjang = $p;
        }

        public function getPanjang(){
            return $this->panjang;
        }
        public function setLebar($l){
            $this->lebar = $l;
        }
        public function getLebar(){
            return $this->lebar;
        }

        public function hitungLuas()
        {
            return $this->getPanjang() * $this->getLebar();
        }

        function hitungKeliling()  
        {
            return 2 * ($this->getPanjang() + $this->getLebar());
        }
    }

    class TrSegi4 extends segiEmpat{
        private $volumeKubus;
        private $volumeBalok;

        function setVolumeKubus($sisi){

            $this->setPanjang($sisi);
            $this->setLebar($sisi);
            $luas = $this->hitungLuas();
            $this->volumeKubus = $luas * $this->getPanjang();
        }

        function getVolumeKubus(){
            return $this->volumeKubus;
        }

        function setVolumeBalok($panjang, $lebar, $tinggi){

            $this->setPanjang($panjang);
            $this->setLebar($lebar);
            $luas = $this->hitungLuas();
            $this->volumeBalok = $luas * $tinggi;
        }

        function getVolumeBalok(){
            return $this->volumeBalok;
        }


    }
    
    class Hasil{
        function tampilTrSegi4($sisi, $panjang, $lebar, $tinggi){
            $hasilVolume = new TrSegi4();
            $hasilVolume->setVolumeKubus($sisi);
            $hasilVolume->setVolumeBalok($panjang, $lebar, $tinggi);
            echo "Volume Kubus dengan panjang sisi $sisi adalah ". $hasilVolume->getVolumeKubus(). "<br>";
            echo "Volume Balok dengan panjang $panjang, lebar $lebar, dan tinggi $tinggi adalah ". $hasilVolume->getVolumeBalok(). "<br>";
        }
    }

    $hasil = new Hasil();
    $hasil->tampilTrSegi4(4,3,4,5);
    
?> 